package ru.tsc.sbagrintsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserViewProfileRequest;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignInResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserViewProfileResponse;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.sbagrintsev.tm.marker.SoapCategory;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());

    @Test
    public void testSignIn() {
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.signIn(new UserSignInRequest("wrong", "wrong"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.signIn(new UserSignInRequest(null, "wrong"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.signIn(new UserSignInRequest("wrong", null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.signIn(new UserSignInRequest(null, null))
        );
        final UserSignInResponse response = authEndpoint.signIn(new UserSignInRequest("test", "test"));
        Assert.assertNotNull(response);
        final String token = response.getToken();
        Assert.assertNotNull(token);
    }

    @Test
    public void testViewProfile() {
        final UserSignInResponse response = authEndpoint.signIn(new UserSignInRequest("test", "test"));
        Assert.assertNotNull(response);
        final String token = response.getToken();
        Assert.assertNotNull(token);
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.viewProfile(new UserViewProfileRequest(null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.viewProfile(new UserViewProfileRequest("wrong"))
        );
        final UserViewProfileResponse profileResponse = authEndpoint.viewProfile(new UserViewProfileRequest(token));
        Assert.assertNotNull(profileResponse);
        Assert.assertEquals("test", profileResponse.getUser().getLogin());
    }

}
