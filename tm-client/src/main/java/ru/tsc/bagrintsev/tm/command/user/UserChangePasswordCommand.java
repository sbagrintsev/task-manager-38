package ru.tsc.bagrintsev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.user.UserChangePasswordRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        System.out.printf("Enter old password: %n");
        @NotNull final String oldPassword = TerminalUtil.nextLine();
        System.out.printf("Enter new password: %n");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setNewPassword(newPassword);
        request.setOldPassword(oldPassword);
        getUserEndpoint().changePassword(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change user password.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-change-password";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
