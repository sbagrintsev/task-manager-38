package ru.tsc.bagrintsev.tm.dto.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UserUpdateProfileRequest extends AbstractUserRequest {

    @Nullable
    private String firstName;

    @Nullable
    private String middleName;

    @Nullable
    private String lastName;

    public UserUpdateProfileRequest(@Nullable String token) {
        super(token);
    }

    public UserUpdateProfileRequest(@Nullable String token, @Nullable String firstName, @Nullable String middleName, @Nullable String lastName) {
        super(token);
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

}
