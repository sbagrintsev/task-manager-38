package ru.tsc.bagrintsev.tm.api.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.*;
import ru.tsc.bagrintsev.tm.dto.response.data.*;

@WebService
public interface IDomainEndpoint extends IAbstractEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IAbstractEndpoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    BackupLoadResponse loadBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final BackupLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    );

    @NotNull
    @WebMethod
    DataBinaryLoadResponse loadBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonJsonLoadResponse loadJacksonJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonJsonLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonXmlLoadResponse loadJacksonXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonXmlLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonYamlLoadResponse loadJacksonYaml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonYamlLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJaxbJsonLoadResponse loadJaxbJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbJsonLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJaxbXmlLoadResponse loadJaxbXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbXmlLoadRequest request
    );

    @NotNull
    @WebMethod
    BackupSaveResponse saveBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final BackupSaveRequest request
    );

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    );

    @NotNull
    @WebMethod
    DataBinarySaveResponse saveBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonJsonSaveResponse saveJacksonJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonJsonSaveRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonXmlSaveResponse saveJacksonXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonXmlSaveRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonYamlSaveResponse saveJacksonYaml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonYamlSaveRequest request
    );

    @NotNull
    @WebMethod
    DataJaxbJsonSaveResponse saveJaxbJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbJsonSaveRequest request
    );

    @NotNull
    @WebMethod
    DataJaxbXmlSaveResponse saveJaxbXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbXmlSaveRequest request
    );

}
