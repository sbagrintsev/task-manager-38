package ru.tsc.bagrintsev.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException(@Nullable final String arg) {
        super(String.format("Error! Command '%s' is not supported...", arg));
    }

}
