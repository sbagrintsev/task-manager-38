package ru.tsc.bagrintsev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.model.IHasDateCreated;
import ru.tsc.bagrintsev.tm.api.model.IWBS;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.util.DateUtil;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractUserOwnedModel extends AbstractModel implements IHasDateCreated, IWBS {

    @Nullable
    private String userId;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date dateCreated = new Date();

    @Nullable
    private Date dateStarted;

    @Nullable
    private Date dateFinished;

    @Override
    public String toString() {
        return name + " : " + description +
                "\n\tid: " + getId() +
                "\n\tstatus: " + status +
                "\tcreated: " + DateUtil.toString(dateCreated) +
                "\tstarted: " + DateUtil.toString(dateStarted) +
                "\tfinished: " + DateUtil.toString(dateFinished);
    }

}
