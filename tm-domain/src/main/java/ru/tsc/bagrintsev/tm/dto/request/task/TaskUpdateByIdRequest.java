package ru.tsc.bagrintsev.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskUpdateByIdRequest(@Nullable String token) {
        super(token);
    }

    public TaskUpdateByIdRequest(@Nullable String token, @Nullable String taskId, @Nullable String name, @Nullable String description) {
        super(token);
        this.taskId = taskId;
        this.name = name;
        this.description = description;
    }

}
