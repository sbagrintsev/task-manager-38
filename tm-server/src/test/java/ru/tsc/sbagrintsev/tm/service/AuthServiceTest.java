package ru.tsc.sbagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.api.sevice.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.service.*;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.security.GeneralSecurityException;

@Category(DBCategory.class)
public final class AuthServiceTest {

    @NotNull
    private IUserService userService;

    @NotNull
    private IAuthService authService;

    @Nullable
    private String token;

    @Before
    public void setUp() throws AbstractException, GeneralSecurityException {
        @NotNull final PropertyService propertyService = new PropertyService();
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        @NotNull final IProjectService projectService = new ProjectService(connectionService);
        @NotNull final ITaskService taskService = new TaskService(connectionService);
        userService = new UserService(projectService, taskService, propertyService, connectionService);
        authService = new AuthService(userService, propertyService);
        userService.create("testLogin", "testPassword");
        token = null;
    }

    @After
    public void tearDown() throws AbstractException {
        userService.removeByLogin("testLogin");
    }

    @Test
    @Category(DBCategory.class)
    public void testSignIn() {
        Assert.assertNull(token);
        token = authService.signIn("testLogin", "testPassword");
        Assert.assertNotNull(token);
    }

    @Test
    @Category(DBCategory.class)
    public void testValidateToken() {
        token = authService.signIn("testLogin", "testPassword");
        Assert.assertNotNull(token);
        Assert.assertNotNull(authService.validateToken(token));
    }

}
