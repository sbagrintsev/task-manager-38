package ru.tsc.sbagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.repository.UserRepository;
import ru.tsc.bagrintsev.tm.service.ConnectionService;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.bagrintsev.tm.util.HashUtil;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Category(DBCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private UserRepository userRepository;

    @Before
    public void setUp() {
        @NotNull final PropertyService propertyService = new PropertyService();
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        userRepository = new UserRepository(connectionService.getConnection());
    }

    @Test
    @Category(DBCategory.class)
    public void testAdd() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        @NotNull final User user = new User();
        userRepository.add(user);
        Assert.assertFalse(userRepository.findAll().isEmpty());
        Assert.assertEquals(user, userRepository.findAll().get(0));
    }

    @Test
    @Category(DBCategory.class)
    public void testClear() throws GeneralSecurityException, SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        final byte @NotNull [] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        @NotNull final User user2 = new User();
        user2.setId("id2");
        user2.setLogin("login2");
        final byte @NotNull [] salt2 = HashUtil.generateSalt();
        @NotNull final String passwordHash2 = HashUtil.generateHash("pass2", salt2, 50, 10);
        user2.setPasswordHash(passwordHash2);
        user2.setPasswordSalt(salt2);
        userRepository.add(user);
        userRepository.add(user2);
        Assert.assertEquals(2, userRepository.findAll().size());
        userRepository.clear();
        Assert.assertEquals(0, userRepository.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testCreate() throws SQLException, AbstractException {
        userRepository.create("testLogin", "testPass", "salt".getBytes());
        Assert.assertFalse(userRepository.findAll().isEmpty());
        Assert.assertEquals("testLogin", userRepository.findAll().get(0).getLogin());
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DBCategory.class)
    public void testExistsById() throws AbstractException, GeneralSecurityException, SQLException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        final byte @NotNull [] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        userRepository.add(user);
        Assert.assertTrue(userRepository.existsById("id1"));
        Assert.assertTrue(userRepository.existsById("id2"));
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAll() throws SQLException, AbstractException {
        userRepository.create("testLogin1", "testPass1", "salt1".getBytes());
        userRepository.create("testLogin2", "testPass2", "salt2".getBytes());
        userRepository.create("testLogin3", "testPass3", "salt3".getBytes());
        Assert.assertEquals(3, userRepository.findAll().size());
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DBCategory.class)
    public void testFindByEmail() throws UserNotFoundException, SQLException, IncorrectRoleException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        user.setEmail("test@email.ru");
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findByEmail("test@email.ru"));
        Assert.assertEquals(user, userRepository.findByEmail("wrong"));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DBCategory.class)
    public void testFindByLogin() throws UserNotFoundException, SQLException, IncorrectRoleException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findByLogin("login1"));
        Assert.assertEquals(user, userRepository.findByLogin("wrong"));
    }

    @Test
    @Category(DBCategory.class)
    public void testFindOneById() throws AbstractException, GeneralSecurityException, SQLException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        final byte @NotNull [] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        @NotNull final User user2 = new User();
        user2.setId("id2");
        user2.setLogin("login2");
        final byte @NotNull [] salt2 = HashUtil.generateSalt();
        @NotNull final String passwordHash2 = HashUtil.generateHash("pass2", salt2, 50, 10);
        user2.setPasswordHash(passwordHash2);
        user2.setPasswordSalt(salt2);
        userRepository.add(user);
        userRepository.add(user2);
        Assert.assertEquals("login1", userRepository.findOneById("id1").getLogin());
        Assert.assertEquals("login2", userRepository.findOneById("id2").getLogin());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemove() throws GeneralSecurityException, SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        final byte @NotNull [] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        userRepository.add(user);
        Assert.assertTrue(userRepository.findAll().contains(user));
        Assert.assertEquals(user, userRepository.remove(user));
        Assert.assertFalse(userRepository.findAll().contains(user));
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveAll() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final List<User> userList = new ArrayList<>();
        @NotNull final User user1 = new User();
        @NotNull final User user2 = new User();
        userList.add(user1);
        userList.add(user2);
        userRepository.add(userList);
        Assert.assertEquals(2, userRepository.findAll().size());
        userRepository.removeAll(userList);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DBCategory.class)
    public void testRemoveById() throws AbstractException, GeneralSecurityException, SQLException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        final byte @NotNull [] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        userRepository.add(user);
        Assert.assertTrue(userRepository.findAll().contains(user));
        Assert.assertEquals(user, userRepository.removeById("id1"));
        Assert.assertFalse(userRepository.findAll().contains(user));
        Assert.assertEquals(user, userRepository.removeById("id1"));
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveByLogin() throws UserNotFoundException, SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findByLogin("login1"));
        Assert.assertEquals(user, userRepository.removeByLogin("login1"));
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test(expected = IncorrectParameterNameException.class)
    @Category(DBCategory.class)
    public void testSetParameter() throws AbstractException, SQLException {
        @NotNull final User user1 = new User();
        Assert.assertNull(user1.getEmail());
        userRepository.setParameter(user1, EntityField.EMAIL, "test@email.ru");
        Assert.assertEquals("test@email.ru", user1.getEmail());
        userRepository.setParameter(user1, EntityField.NAME, "error");
    }

    @Test
    @Category(DBCategory.class)
    public void testSetRole() throws SQLException, AbstractException {
        @NotNull final User user = new User();
        Assert.assertEquals(Role.REGULAR, user.getRole());
        userRepository.setRole(user, Role.ADMIN);
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    @Category(DBCategory.class)
    public void testSetUserPassword() throws GeneralSecurityException, SQLException, AbstractException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        final byte @NotNull [] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        Assert.assertNull(user.getPasswordHash());
        Assert.assertNull(user.getPasswordSalt());
        userRepository.setUserPassword(user, passwordHash1, salt1);
        Assert.assertEquals(salt1, user.getPasswordSalt());
        Assert.assertEquals(passwordHash1, user.getPasswordHash());
    }

    @Test
    @Category(DBCategory.class)
    public void testTotalCount() throws GeneralSecurityException, SQLException {
        @NotNull final User user = new User();
        user.setId("id1");
        user.setLogin("login1");
        final byte @NotNull [] salt1 = HashUtil.generateSalt();
        @NotNull final String passwordHash1 = HashUtil.generateHash("pass1", salt1, 50, 10);
        user.setPasswordHash(passwordHash1);
        user.setPasswordSalt(salt1);
        @NotNull final User user2 = new User();
        user2.setId("id2");
        user2.setLogin("login2");
        final byte @NotNull [] salt2 = HashUtil.generateSalt();
        @NotNull final String passwordHash2 = HashUtil.generateHash("pass2", salt2, 50, 10);
        user2.setPasswordHash(passwordHash2);
        user2.setPasswordSalt(salt2);
        userRepository.add(user);
        userRepository.add(user2);
        Assert.assertEquals(2, userRepository.totalCount());
    }

}
