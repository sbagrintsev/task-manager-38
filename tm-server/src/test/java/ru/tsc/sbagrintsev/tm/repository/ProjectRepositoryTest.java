package ru.tsc.sbagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.service.ConnectionService;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Category(DBCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final String userId = "testUserId1";

    @NotNull
    private final String userId2 = "testUserId2";

    @NotNull
    private ProjectRepository projectRepository;

    @Before
    public void setUp() {
        @NotNull final PropertyService propertyService = new PropertyService();
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        projectRepository = new ProjectRepository(connectionService.getConnection());
    }

    @Test
    @Category(DBCategory.class)
    public void testAdd() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final Project project = new Project();
        projectRepository.add(userId, project);
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertEquals(project, projectRepository.findAll().get(0));
        Assert.assertEquals(userId, projectRepository.findAll().get(0).getUserId());
    }

    @Test
    @Category(DBCategory.class)
    public void testAddCollection() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        projectList.add(project1);
        projectList.add(project2);
        projectRepository.add(projectList);
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertEquals(2, projectRepository.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testClear() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectRepository.add(userId, project);
        projectRepository.add(userId, project2);
        @NotNull final Project project3 = new Project();
        project.setId("id3");
        project.setName("name3");
        projectRepository.add(userId2, project3);
        Assert.assertEquals(2, projectRepository.findAll(userId).size());
        Assert.assertEquals(1, projectRepository.findAll(userId2).size());
        projectRepository.clear(userId);
        Assert.assertEquals(0, projectRepository.findAll(userId).size());
        Assert.assertEquals(1, projectRepository.findAll(userId2).size());
    }

    @Test
    @Category(DBCategory.class)
    public void testCreate() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        projectRepository.create(userId, "name1");
        projectRepository.create(userId, "name2", "description2");
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertEquals("name1", projectRepository.findAll().get(0).getName());
        Assert.assertEquals("description2", projectRepository.findAll().get(1).getDescription());
        Assert.assertEquals(userId, projectRepository.findAll().get(0).getUserId());
        Assert.assertEquals(userId, projectRepository.findAll().get(1).getUserId());
    }

    @Test(expected = ModelNotFoundException.class)
    @Category(DBCategory.class)
    public void testExistsById() throws AbstractException, SQLException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectRepository.add(userId, project);
        Assert.assertTrue(projectRepository.existsById(userId, "id1"));
        Assert.assertTrue(projectRepository.existsById(userId, "id2"));
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAll() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        projectRepository.create(userId, "name1");
        projectRepository.create(userId, "name2");
        projectRepository.create(userId2, "name3");
        Assert.assertEquals(2, projectRepository.findAll(userId).size());
        Assert.assertEquals(1, projectRepository.findAll(userId2).size());
        Assert.assertEquals(3, projectRepository.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAllSorted() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        projectRepository.create(userId, "name8");
        projectRepository.create(userId, "name6");
        projectRepository.create(userId2, "name3");
        projectRepository.create(userId2, "name1");
        Assert.assertEquals("name6", projectRepository.findAll(userId, Sort.BY_NAME.getComparator()).get(0).getName());
        Assert.assertEquals("name3", projectRepository.findAll(userId2, Sort.BY_CREATED.getComparator()).get(0).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindOneById() throws AbstractException, SQLException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectRepository.add(userId, project);
        projectRepository.add(userId, project2);
        Assert.assertEquals("name1", projectRepository.findOneById(userId, "id1").getName());
        Assert.assertEquals("name2", projectRepository.findOneById(userId, "id2").getName());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemove() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectRepository.add(userId, project);
        Assert.assertTrue(projectRepository.findAll(userId).contains(project));
        Assert.assertEquals(project, projectRepository.remove(userId, project));
        Assert.assertFalse(projectRepository.findAll(userId).contains(project));
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveAll() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        projectList.add(project1);
        projectList.add(project2);
        projectRepository.add(projectList);
        Assert.assertEquals(2, projectRepository.findAll().size());
        projectRepository.removeAll(projectList);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test(expected = ModelNotFoundException.class)
    @Category(DBCategory.class)
    public void testRemoveById() throws AbstractException, SQLException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectRepository.add(userId, project);
        Assert.assertTrue(projectRepository.findAll(userId).contains(project));
        Assert.assertEquals(project, projectRepository.removeById(userId, "id1"));
        Assert.assertFalse(projectRepository.findAll(userId).contains(project));
        Assert.assertEquals(project, projectRepository.removeById(userId, "id1"));
    }

    @Test
    @Category(DBCategory.class)
    public void testTotalCount() throws SQLException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectRepository.add(userId, project);
        projectRepository.add(userId, project2);
        Assert.assertEquals(2, projectRepository.totalCount());
    }

}
