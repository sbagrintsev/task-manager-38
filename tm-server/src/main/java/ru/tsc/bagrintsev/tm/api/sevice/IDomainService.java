package ru.tsc.bagrintsev.tm.api.sevice;

public interface IDomainService {

    void loadBackup();

    void loadBase64();

    void loadBinary();

    void loadJacksonJSON();

    void loadJacksonXML();

    void loadJacksonYAML();

    void loadJaxbJSON();

    void loadJaxbXML();

    void saveBackup();

    void saveBase64();

    void saveBinary();

    void saveJacksonJSON();

    void saveJacksonXML();

    void saveJacksonYAML();

    void saveJaxbJSON();

    void saveJaxbXML();

}
