package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.sql.SQLException;

public interface IProjectTaskService extends Checkable {

    Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException, SQLException;

    void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException, SQLException;

    Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws AbstractException;

}
