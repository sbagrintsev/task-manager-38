package ru.tsc.bagrintsev.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.*;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.user.EmailAlreadyExistsException;
import ru.tsc.bagrintsev.tm.exception.user.LoginAlreadyExistsException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.repository.UserRepository;
import ru.tsc.bagrintsev.tm.util.HashUtil;

import java.security.GeneralSecurityException;
import java.sql.Connection;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @Getter
    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService
    ) {
        super(connectionService);
        this.projectService = projectService;
        this.taskService = taskService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User checkUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException, GeneralSecurityException {
        check(EntityField.LOGIN, login);
        check(EntityField.PASSWORD, password);
        @NotNull final User user = findByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();
        @NotNull final Integer iterations = propertyService.getPasswordHashIterations();
        @NotNull final Integer keyLength = propertyService.getPasswordHashKeyLength();
        if (!(HashUtil.generateHash(password, user.getPasswordSalt(), iterations, keyLength)).equals(user.getPasswordHash())) {
            throw new PasswordIsIncorrectException();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws GeneralSecurityException, AbstractException {
        check(EntityField.LOGIN, login);
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        check(EntityField.PASSWORD, password);
        @NotNull final Integer iterations = getPropertyService().getPasswordHashIterations();
        @NotNull final Integer keyLength = getPropertyService().getPasswordHashKeyLength();
        final byte @NotNull [] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(password, salt, iterations, keyLength);
        @NotNull final Connection connection = getConnection();
        @NotNull User user;
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.create(login, passwordHash, salt);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) throws AbstractException {
        check(EntityField.EMAIL, email);
        @NotNull User user;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findByEmail(email);
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) throws AbstractException {
        check(EntityField.LOGIN, login);
        @NotNull User user;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findByLogin(login);
        }
        return user;
    }

    @NotNull
    public IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        try {
            findByEmail(email);
        } catch (AbstractException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        try {
            findByLogin(login);
        } catch (AbstractException e) {
            return false;
        }
        return true;
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) throws AbstractException {
        check(EntityField.LOGIN, login);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull final User user = repository.findByLogin(login);
            repository.setParameter(user, EntityField.LOCKED, "true");
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) throws AbstractException {
        check(EntityField.LOGIN, login);
        @NotNull User user;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.removeByLogin(login);
            @NotNull final String userId = user.getId();
            taskService.clear(userId);
            projectService.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setParameter(
            @Nullable final User user,
            @Nullable final EntityField paramName,
            @Nullable final String paramValue
    ) {
        check(Entity.USER, user);
        if (EntityField.EMAIL.equals(paramName) && isEmailExists(paramValue)) {
            throw new EmailAlreadyExistsException(paramValue);
        }
        @NotNull User userResult;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            userResult = repository.setParameter(user, paramName, paramValue);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return userResult;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(
            @Nullable final String userId,
            @Nullable final String newPassword,
            @Nullable final String oldPassword
    ) {
        check(EntityField.ID, userId);
        check(EntityField.PASSWORD, newPassword);
        check(EntityField.PASSWORD, oldPassword);
        @NotNull final Integer iterations = getPropertyService().getPasswordHashIterations();
        @NotNull final Integer keyLength = getPropertyService().getPasswordHashKeyLength();
        final byte @NotNull [] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(newPassword, salt, iterations, keyLength);
        @NotNull User user;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findOneById(userId);
            checkUser(user.getLogin(), oldPassword);
            user = repository.setUserPassword(user, passwordHash, salt);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setRole(
            @Nullable final String login,
            @Nullable final Role role
    ) {
        check(EntityField.LOGIN, login);
        check(Entity.ROLE, role);
        @NotNull User user;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findByLogin(login);
            user = repository.setRole(user, role);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        check(EntityField.LOGIN, login);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull final User user = repository.findByLogin(login);
            repository.setParameter(user, EntityField.LOCKED, "false");
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        check(EntityField.ID, userId);
        @NotNull User user;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findOneById(userId);
            repository.setParameter(user, EntityField.FIRST_NAME, firstName);
            repository.setParameter(user, EntityField.MIDDLE_NAME, middleName);
            repository.setParameter(user, EntityField.LAST_NAME, lastName);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

}
