package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseUserName();

    @NotNull
    String getDatabaseUserPassword();

    @NotNull
    Integer getPasswordHashIterations();

    @NotNull
    Integer getPasswordHashKeyLength();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}
