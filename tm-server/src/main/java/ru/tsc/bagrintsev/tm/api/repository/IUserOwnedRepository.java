package ru.tsc.bagrintsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

    @NotNull
    M add(
            @NotNull final String userId,
            @NotNull final M record
    ) throws AbstractException, SQLException;

    @NotNull
    M changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException, SQLException;

    void clear(@NotNull final String userId) throws AbstractException, SQLException;

    @NotNull
    M create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException;

    @NotNull
    M create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException;

    boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException, SQLException;

    @NotNull
    List<M> findAll(@NotNull final String userId) throws AbstractException, SQLException;

    @NotNull
    List<M> findAll(
            @NotNull final String userId,
            @NotNull final Comparator comparator
    ) throws AbstractException, SQLException;

    @NotNull
    M findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException, SQLException;

    @NotNull
    M remove(
            @NotNull final String userId,
            @NotNull final M record
    ) throws AbstractException, SQLException;

    @NotNull
    M removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException, SQLException;

    long totalCount(@NotNull final String userId) throws AbstractException, SQLException;

    @NotNull
    M updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException, SQLException;

}
