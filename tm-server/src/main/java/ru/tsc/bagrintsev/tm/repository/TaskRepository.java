package ru.tsc.bagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(Task.class, connection);
    }

    @NotNull
    @Override
    public Task add(@NotNull final Task task) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, date_created, name, description, status, user_id, project_id, date_started, date_finished) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setTimestamp(2, javaDateToSql(task.getDateCreated()));
            statement.setString(3, task.getName());
            statement.setString(4, task.getDescription());
            statement.setString(5, task.getStatus().toString());
            statement.setString(6, task.getUserId());
            statement.setString(7, task.getProjectId());
            statement.setTimestamp(8, javaDateToSql(task.getDateStarted()));
            statement.setTimestamp(9, javaDateToSql(task.getDateFinished()));
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    protected Task fetch(@NotNull final ResultSet rowSet) throws SQLException, IncorrectStatusException {
        @NotNull final Task task = new Task();
        task.setId(rowSet.getString("id"));
        task.setDateCreated(rowSet.getTimestamp("date_created"));
        task.setName(rowSet.getString("name"));
        task.setDescription(rowSet.getString("description"));
        task.setStatus(Status.toStatus(rowSet.getString("status")));
        task.setUserId(rowSet.getString("user_id"));
        task.setDateStarted(rowSet.getTimestamp("date_started"));
        task.setDateFinished(rowSet.getTimestamp("date_finished"));
        task.setProjectId(rowSet.getString("project_id"));
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException, IncorrectStatusException {
        @NotNull List<Task> tasks = new ArrayList<>();
        @NotNull final String sql = String.format("" +
                "SELECT * " +
                "FROM %s " +
                "WHERE user_id = ? " +
                "AND project_id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) tasks.add(fetch(rowSet));
        }
        return tasks;
    }

    @NotNull
    @Override
    protected String getTableName() {
        return "m_task";
    }

    @NotNull
    @Override
    public Task setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) throws AbstractException, SQLException {
        @NotNull final String sql = String.format("" +
                "UPDATE %s " +
                "SET    project_id = ? " +
                "WHERE user_id = ? " +
                "AND id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            statement.setString(2, userId);
            statement.setString(3, taskId);
            statement.executeUpdate();
        }
        return findOneById(userId, taskId);
    }

}
