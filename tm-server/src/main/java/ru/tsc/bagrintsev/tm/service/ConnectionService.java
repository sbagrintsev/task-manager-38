package ru.tsc.bagrintsev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    final IPropertyService propertyService;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public @NotNull Connection getConnection() {
        @NotNull final String userName = propertyService.getDatabaseUserName();
        @NotNull final String userPassword = propertyService.getDatabaseUserPassword();
        @NotNull final String url = propertyService.getDatabaseUrl();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        return connection;
    }

}
