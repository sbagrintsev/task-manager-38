package ru.tsc.bagrintsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull final M record) throws AbstractException, SQLException;

    @NotNull
    Collection<M> add(@NotNull final Collection<M> records) throws SQLException;

    void clear() throws SQLException;

    boolean existsById(@NotNull final String id) throws AbstractException, SQLException;

    @NotNull
    List<M> findAll() throws SQLException, IncorrectStatusException, IncorrectRoleException;

    @NotNull
    List<M> findAll(@NotNull final Comparator comparator) throws AbstractException, SQLException;

    @NotNull
    M findOneById(@NotNull final String id) throws AbstractException, SQLException;

    @NotNull
    M remove(@NotNull final M model) throws AbstractException, SQLException;

    void removeAll(@NotNull final Collection<M> collection) throws SQLException;

    @NotNull
    M removeById(@NotNull final String id) throws AbstractException, SQLException;

    @NotNull
    Collection<M> set(@NotNull final Collection<M> records) throws SQLException;

    int totalCount() throws SQLException;

}
