package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.Session;

public interface IAuthService extends Checkable {

    @NotNull
    String signIn(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    Session validateToken(@Nullable String token);

}
