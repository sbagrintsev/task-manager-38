package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}
