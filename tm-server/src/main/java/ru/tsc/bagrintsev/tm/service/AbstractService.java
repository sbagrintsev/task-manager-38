package ru.tsc.bagrintsev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IAbstractRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IAbstractService;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>> implements IAbstractService<M> {

    @NotNull
    private final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M add(@Nullable final M record) throws AbstractException {
        check(Entity.ABSTRACT, record);
        @NotNull M recordResult;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            recordResult = repository.add(record);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return recordResult;
    }

    @NotNull
    @SneakyThrows
    public Collection<M> add(@NotNull final Collection<M> records) {
        @NotNull Collection<M> recordsResult;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            recordsResult = repository.add(records);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return recordsResult;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) throws AbstractException {
        check(EntityField.ID, id);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator comparator) {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            if (comparator == null) {
                return repository.findAll();
            }
            return repository.findAll(comparator);
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) {
            return findAll();
        }
        return findAll(sort.getComparator());
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) throws AbstractException {
        check(EntityField.ID, id);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            return repository.findOneById(id);
        }
    }

    @NotNull
    @SneakyThrows
    public Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract IAbstractRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public M remove(@Nullable final M record) throws AbstractException {
        check(Entity.ABSTRACT, record);
        @NotNull M recordResult;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            recordResult = repository.remove(record);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return recordResult;
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            repository.removeAll(collection);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String id) throws AbstractException {
        check(EntityField.ID, id);
        @NotNull M record;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            record = repository.removeById(id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return record;
    }

    @NotNull
    @SneakyThrows
    public Collection<M> set(@NotNull final Collection<M> records) {
        @NotNull Collection<M> recordsResult;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            recordsResult = repository.set(records);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return recordsResult;
    }

    @Override
    @SneakyThrows
    public int totalCount() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            return repository.totalCount();
        }
    }

}
