package ru.tsc.bagrintsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;
import java.sql.SQLException;

public interface IUserRepository extends IAbstractRepository<User> {

    @NotNull
    User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final byte[] salt
    ) throws GeneralSecurityException, SQLException, AbstractException;

    @NotNull
    User findByEmail(@NotNull final String email) throws UserNotFoundException, SQLException, IncorrectRoleException;

    @NotNull
    User findByLogin(@NotNull final String login) throws UserNotFoundException, SQLException, IncorrectRoleException;

    @NotNull
    User removeByLogin(@NotNull final String login) throws UserNotFoundException, SQLException, IncorrectRoleException;

    @NotNull
    User setParameter(
            @NotNull final User user,
            @NotNull final EntityField paramName,
            @NotNull final String paramValue
    ) throws AbstractException, SQLException;

    @NotNull
    User setRole(
            @NotNull final User user,
            @NotNull final Role role
    ) throws SQLException, AbstractException;

    User setUserPassword(
            @NotNull final User user,
            @NotNull final String password,
            @NotNull byte[] salt
    ) throws GeneralSecurityException, SQLException, AbstractException;

}
