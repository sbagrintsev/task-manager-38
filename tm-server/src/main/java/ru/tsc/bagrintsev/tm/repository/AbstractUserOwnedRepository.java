package ru.tsc.bagrintsev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.bagrintsev.tm.comparator.DateCreatedComparator;
import ru.tsc.bagrintsev.tm.comparator.DateStartedComparator;
import ru.tsc.bagrintsev.tm.comparator.NameComparator;
import ru.tsc.bagrintsev.tm.comparator.StatusComparator;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    private final Class<M> clazz;

    public AbstractUserOwnedRepository(
            @NotNull final Class<M> clazz,
            @NotNull final Connection connection
    ) {
        super(connection);
        this.clazz = clazz;
    }

    @NotNull
    @Override
    public M add(
            @NotNull final String userId,
            @NotNull final M record
    ) throws SQLException {
        record.setUserId(userId);
        return add(record);
    }

    @NotNull
    @Override
    public M changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws SQLException, AbstractException {
        @NotNull String sql = "";
        if (status.equals(Status.IN_PROGRESS)) {
            sql = String.format("" +
                    "UPDATE %s " +
                    "SET    status = ? ," +
                    "       date_started = current_timestamp " +
                    "WHERE user_id = ? " +
                    "AND id = ?;", getTableName());
        } else if (status.equals(Status.COMPLETED)) {
            sql = String.format("" +
                    "UPDATE %s " +
                    "SET    status = ? ," +
                    "       date_finished = current_timestamp " +
                    "WHERE user_id = ? " +
                    "AND id = ?;", getTableName());
        } else if (status.equals(Status.NOT_STARTED)) {
            sql = String.format("" +
                    "UPDATE %s " +
                    "SET    status = ? ," +
                    "       date_started = null ," +
                    "       date_finished = null " +
                    "WHERE user_id = ? " +
                    "AND id = ?;", getTableName());
        }
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, status.toString());
            statement.setString(2, userId);
            statement.setString(3, id);
            statement.executeUpdate();
        }
        return findOneById(userId, id);
    }

    @Override
    public void clear(@NotNull final String userId) throws SQLException {
        @NotNull final String sql = String.format("" +
                "DELETE FROM %s " +
                "WHERE user_id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final M record = clazz.getDeclaredConstructor().newInstance();
        record.setName(name);
        return add(userId, record);
    }

    @NotNull
    @Override
    @SneakyThrows
    public M create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final M record = clazz.getDeclaredConstructor().newInstance();
        record.setName(name);
        record.setDescription(description);
        return add(userId, record);
    }

    @Override
    public boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException, SQLException {
        findOneById(userId, id);
        return true;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) throws SQLException, IncorrectStatusException, IncorrectRoleException {
        return findAll(userId, DateCreatedComparator.INSTANCE);
    }

    @NotNull
    @Override
    public List<M> findAll(
            @NotNull final String userId,
            @NotNull final Comparator comparator
    ) throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull List<M> records = new ArrayList<>();
        @NotNull String order = getQueryOrder(comparator);
        @NotNull final String sql = String.format(
                "SELECT * " +
                        "FROM %s " +
                        "WHERE user_id = ? " +
                        "ORDER BY %s;", getTableName(), order);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) records.add(fetch(rowSet));
        }
        return records;
    }

    @NotNull
    @Override
    public M findOneById(
            final @NotNull String userId,
            final @NotNull String id
    ) throws AbstractException, SQLException {
        @NotNull final String sql = String.format("" +
                "SELECT * " +
                "FROM %s " +
                "WHERE user_id = ? " +
                "AND id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) throw new ModelNotFoundException();
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    public M remove(
            @NotNull final String userId,
            @NotNull final M record
    ) throws SQLException {
        @NotNull final String sql = String.format("" +
                "DELETE FROM %s " +
                "WHERE user_id = ? " +
                "AND id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, record.getId());
            statement.executeUpdate();
        }
        return record;
    }

    @NotNull
    @Override
    public M removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException, SQLException {
        @NotNull final M record = findOneById(userId, id);
        @NotNull final String sql = String.format("" +
                "DELETE FROM %s " +
                "WHERE user_id = ? " +
                "AND id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            statement.executeUpdate();
        }
        return record;
    }

    @Override
    public long totalCount(@NotNull final String userId) throws SQLException {
        @NotNull final String sql = String.format("" +
                "SELECT COUNT(*) as cnt " +
                "FROM %s " +
                "WHERE user_id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            return rowSet.getInt("cnt");
        }
    }

    @Override
    public @NotNull M updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException, SQLException {
        @NotNull final String sql = String.format("" +
                "UPDATE %s " +
                "SET    name = ? , " +
                "       description = ? " +
                "WHERE user_id = ? " +
                "AND id = ?;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, name);
            statement.setString(2, description);
            statement.setString(3, userId);
            statement.setString(4, id);
            statement.executeUpdate();
        }
        return findOneById(userId, id);
    }

}
