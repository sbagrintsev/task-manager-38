package ru.tsc.bagrintsev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.IUserOwnedService;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    @SneakyThrows
    public M add(
            @Nullable final String userId,
            @Nullable final M record
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(Entity.ABSTRACT, record);
        @NotNull M recordResult;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            recordResult = repository.add(userId, record);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return recordResult;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        check(Entity.STATUS, status);
        @NotNull M record;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            record = repository.changeStatusById(userId, id, status);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return record;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.NAME, name);
        @NotNull M record;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            record = repository.create(userId, name);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return record;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.NAME, name);
        check(EntityField.DESCRIPTION, description);
        @NotNull M record;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            record = repository.create(userId, name, description);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return record;
    }

    @Override
    @SneakyThrows
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.existsById(userId, id);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            List<M> list = repository.findAll(userId);
            return list == null ? Collections.emptyList() : list;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        if (comparator == null) {
            return findAll(userId);
        }
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId, comparator);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        if (sort == null) {
            return findAll(userId);
        }
        return findAll(userId, (Comparator<M>) sort.getComparator());
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findOneById(userId, id);
        }
    }

    @NotNull
    protected abstract IUserOwnedRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public M remove(
            @Nullable final String userId,
            @Nullable final M record
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(Entity.ABSTRACT, record);
        @NotNull M recordResult;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            recordResult = repository.remove(userId, record);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return recordResult;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        @NotNull M record;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            record = repository.removeById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return record;
    }

    @Override
    @SneakyThrows
    public long totalCount(@Nullable final String userId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.totalCount(userId);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        check(EntityField.NAME, name);
        check(EntityField.DESCRIPTION, description);
        @NotNull M record;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            record = repository.updateById(userId, id, name, description);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return record;
    }

}
