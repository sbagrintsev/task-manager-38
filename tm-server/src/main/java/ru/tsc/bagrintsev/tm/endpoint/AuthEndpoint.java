package ru.tsc.bagrintsev.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignOutRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserViewProfileRequest;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignInResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignOutResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserViewProfileResponse;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.model.Session;
import ru.tsc.bagrintsev.tm.model.User;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserSignInResponse signIn(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserSignInRequest request
    ) {
        @NotNull final IAuthService authService = getAuthService();
        @NotNull final String token = authService.signIn(request.getLogin(), request.getPassword());
        return new UserSignInResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserSignOutResponse signOut(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserSignOutRequest request
    ) {
        check(request, Role.values());
        return new UserSignOutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserViewProfileResponse viewProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserViewProfileRequest request
    ) {

        @Nullable final Session session = check(request, Role.values());
        @Nullable final User user = getServiceLocator().getUserService().findOneById(session.getUserId());
        return new UserViewProfileResponse(user);
    }

}
