package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IAbstractRepository;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.List;

public interface IAbstractService<M extends AbstractModel> extends IAbstractRepository<M>, Checkable {

    @NotNull
    List<M> findAll(@Nullable final Sort sort);

}
