package ru.tsc.bagrintsev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.sevice.IDomainService;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.dto.Domain;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Base64;

public final class DomainService implements IDomainService {

    @NotNull
    public static final String FILE_BACKUP = "./backup.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_JACKSON_XML = "./data.jackson.xml";

    @NotNull
    public static final String FILE_JACKSON_JSON = "./data.jackson.json";

    @NotNull
    public static final String FILE_JACKSON_YAML = "./data.jackson.yaml";

    @NotNull
    public static final String FILE_JAXB_XML = "./data.jaxb.xml";

    @NotNull
    public static final String FILE_JAXB_JSON = "./data.jaxb.json";

    @NotNull
    private final IServiceLocator serviceLocator;

    public DomainService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public Domain getDomain() throws SQLException, IncorrectStatusException, IncorrectRoleException {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) throws SQLException {
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
    }

    @Override
    @SneakyThrows
    public void loadBackup() {
        @NotNull final String base64 = new String(Files.readAllBytes(Paths.get(FILE_BACKUP)));
        final byte @NotNull [] bytes = Base64.getDecoder().decode(base64);
        try (@NotNull final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
             @NotNull final ObjectInputStream ois = new ObjectInputStream(bais)) {
            @NotNull final Domain domain = (Domain) ois.readObject();
            setDomain(domain);
        }
    }

    @Override
    @SneakyThrows
    public void loadBase64() {
        @NotNull final String base64 = new String(Files.readAllBytes(Paths.get(FILE_BASE64)));
        final byte @NotNull [] bytes = Base64.getDecoder().decode(base64);
        try (@NotNull final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
             @NotNull final ObjectInputStream ois = new ObjectInputStream(bais)) {
            @NotNull final Domain domain = (Domain) ois.readObject();
            setDomain(domain);
        }
    }

    @Override
    @SneakyThrows
    public void loadBinary() {
        try (@NotNull final ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILE_BINARY))) {
            @NotNull final Domain domain = (Domain) ois.readObject();
            setDomain(domain);
        }
    }

    @Override
    @SneakyThrows
    public void loadJacksonJSON() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_JACKSON_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadJacksonXML() {
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_JACKSON_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadJacksonYAML() {
        @NotNull final String yaml = new String(Files.readAllBytes(Paths.get(FILE_JACKSON_YAML)));
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadJaxbJSON() {
        @NotNull final File file = new File(FILE_JAXB_JSON);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty("eclipselink.media-type", "application/json");
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadJaxbXML() {
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveBackup() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BACKUP);
        try (@NotNull final ByteArrayOutputStream baos = new ByteArrayOutputStream();
             @NotNull final ObjectOutputStream oos = new ObjectOutputStream(baos);
             @NotNull final FileOutputStream fos = new FileOutputStream(file, false)) {
            oos.writeObject(domain);
            final byte @NotNull [] bytes = baos.toByteArray();
            @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
            fos.write(base64.getBytes());
            fos.flush();
        }
    }

    @Override
    @SneakyThrows
    public void saveBase64() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        try (@NotNull final ByteArrayOutputStream baos = new ByteArrayOutputStream();
             @NotNull final ObjectOutputStream oos = new ObjectOutputStream(baos);
             @NotNull final FileOutputStream fos = new FileOutputStream(file, false)) {
            oos.writeObject(domain);
            final byte @NotNull [] bytes = baos.toByteArray();
            @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
            fos.write(base64.getBytes());
            fos.flush();
        }
    }

    @Override
    @SneakyThrows
    public void saveBinary() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        try (@NotNull final ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file, false))) {
            oos.writeObject(domain);
        }
    }

    @Override
    @SneakyThrows
    public void saveJacksonJSON() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (@NotNull final FileOutputStream fos = new FileOutputStream(FILE_JACKSON_JSON)) {
            fos.write(json.getBytes());
            fos.flush();
        }
    }

    @Override
    @SneakyThrows
    public void saveJacksonXML() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (@NotNull final FileOutputStream fos = new FileOutputStream(FILE_JACKSON_XML)) {
            fos.write(xml.getBytes());
            fos.flush();
        }
    }

    @Override
    @SneakyThrows
    public void saveJacksonYAML() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (@NotNull final FileOutputStream fos = new FileOutputStream(FILE_JACKSON_YAML)) {
            fos.write(yaml.getBytes());
            fos.flush();
        }
    }

    @Override
    @SneakyThrows
    public void saveJaxbJSON() {
        System.setProperty("jakarta.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JAXB_JSON);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty("eclipselink.media-type", "application/json");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, file);
    }

    @Override
    @SneakyThrows
    public void saveJaxbXML() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, file);
    }

}
