package ru.tsc.bagrintsev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.user.PermissionDeniedException;
import ru.tsc.bagrintsev.tm.model.Session;
import ru.tsc.bagrintsev.tm.model.User;

import java.util.Arrays;

@NoArgsConstructor
public class AbstractEndpoint {

    @Getter
    @Nullable
    protected IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @SneakyThrows
    protected Session check(
            @NotNull final AbstractUserRequest request,
            @NotNull final Role role
    ) {
        return check(request, new Role[]{role});
    }

    @SneakyThrows
    protected Session check(
            @NotNull final AbstractUserRequest request,
            @NotNull final Role[] roles
    ) {
        @Nullable String token = request.getToken();
        @NotNull final Session session = getServiceLocator().getAuthService().validateToken(token);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new AccessDeniedException();
        @NotNull final User user = getServiceLocator().getUserService().findOneById(userId);
        @NotNull final Role userRole = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(userRole);
        if (!hasRole) throw new PermissionDeniedException();
        return session;
    }

}
